const express = require('express');
const Movie = require('../models/movie');
var log4js = require("log4js");
var logger = log4js.getLogger();

// RESTFULL => GET, POST, PUT, PATCH, DELETE = Modelo
// representacion de una estructura de datos
function list(req, res, next) {
  Movie.find().populate("_actors").then(objs =>{
    logger.level = "info";
    logger.info(res.__('list.movies'));
    res.status(200).json({
    message: res.__('list.movies'),
    obj: objs
  })})
  .catch(
    ex => {
    logger.level="error";
    logger.error(res.__('badList.movies'));
    res.status(500).json({
    message: res.__ ('badList.movies'),
    obj: ex
  })}
  );
}

function index(req, res, next) {
  const id = req.params.id;
  Movie.findOne({"_id":id}).populate("_actors").then(obj => {
    logger.level = "info";
    logger.info(res.__('id.movies'));
    res.status(200).json({
    message: res.__('id.movies'),
    obj: obj
  })})
  .catch(
    ex =>{
    logger.level="error";
    logger.error(res.__('badId.movies'));
    res.status(500).json({
    message: res.__('badId.movies'),
    obj: ex
  })}
  );
}


function create(req, res, next){
  const {
    genre,
    title,
    name,
    lastName,
    actors
  } = req.body;

  const director = new Object({
    _name: name,
    _lastName: lastName
  });

  let movie = new Movie({
    genre: genre,
    title: title,
    actors: actors,
    director: director
  });

  movie.save().then(obj =>{
      logger.level = "info";
      logger.info(res.__('create.movies'));
      res.status(200).json({ 
      message: res.__('create.movies'),
      obj: obj
    })})
    .catch(
      ex => {
      logger.level="error";
      logger.error(res.__('badCreate.movies'));
      res.status(500).json({
      message: res.__('badCreate.movies'),
      obj: ex
    })}
    );
  }

function replace(req, res, next) {
  const id = req.params.id;
  const genre = req.body.genre ? req.body.genre : "";
  const title = req.body.title ? req.body.title : "";
  const name = req.body.name ? req.body.name : "";
  const lastName = req.body.lastName ? req.body.lastName : object.lastName;
  const actors = req.body.actors ? req.body.actors : "";

  const director = new Object({
    _name: name,
    _lastName: lastName
  });

  let movie = new Object({
    _genre: genre,
    _title: title,
    _director: director,
    _actors: actors
  });

  Movie.findOneAndUpdate({"_id":id}, movie).then(obj => 
    {
      logger.level = "info";
      logger.info(res.__('another.movies'));
      res.status(200).json({
      message: res.__('another.movies') ,
      obj: obj
    })})
    .catch(ex => {
      logger.level = "error";
      logger.info(res.__('badAnother.movies'));
      res.status(500).json({
      message: res.__('badAnother.movies'),
      obj: ex
    })}
    );
  }
  

async function edit(req, res, next) {
  const id = req.params.id;
  const {
    genre,
    title,
    name,
    lastName,
    actors
  } = req.body;

  const movie = await Movie.findOne({"_id":id});

  if(genre) {
    movie._genre = genre;
  }
  if(title) {
    movie._title = title;
  }
  if(name) {
    movie._name = name;
  }
  if(lastName) {
    movie._lastName = lastName;
  }
  if(actors) {
    movie._actors = actors;
  }

  movie.save().then(
    obj =>{
      logger.level = "info";
      logger.info(res.__('edit.movies'));
      res.status(200).json({
      message: res.__('edit.movies'),
      obj: obj
    })})
    .catch(
      ex => {
      logger.level="error";
      logger.error(res.__('badEdit.movies'));
      res.status(500).json({
      message: res.__ ('badEdit.movies'),
      obj: ex
    })}
    );
  }
  
function destroy(req, res, next) {
  const id = req.params.id;
  Movie.remove({"_id":id}).then(obj =>{
    logger.level = "info";
      logger.info(res.__('delete.movies'));
      res.status(200).json({
      message: res.__('delete.movies'),
      obj: obj
    })})
    .catch(
      ex => {
      logger.level="error";
      logger.error(res.__('badDelete.movies'));
      res.status(500).json({
      message: res.__ ('badDelete.movies'),
      obj: ex
    })}
    );
  }
module.exports = {
  list, index, create, replace, edit, destroy
}
