const express = require('express');
const Copy = require('../models/copy');
var log4js = require("log4js");
var logger = log4js.getLogger();

// RESTFULL => GET, POST, PUT, PATCH, DELETE = Modelo
// representacion de una estructura de datos
function list(req, res, next) {
  Copy.find().populate("_movie").then(
    objs =>{
      logger.level = "info";
      logger.info(res.__('list.copies'));
      res.status(200).json({
      message: res.__('list.copies'),
      obj: objs
    })})
    .catch(
      ex => {
      logger.level="error";
      logger.error(res.__('badList.copies'));
      res.status(500).json({
      message: res.__ ('badList.copies'),
      obj: ex
    })}
    );
}  

function index(req, res, next) {
  const id = req.params.id;
  Copy.findOne({"_id":id}).populate("_movie").then(obj => {
    logger.level = "info";
    logger.info(res.__('id.copies'));
    res.status(200).json({
    message: res.__('id.copies'),
    obj: obj
  })})
  .catch(
    ex =>{
    logger.level="error";
    logger.error(res.__('badId.copies'));
    res.status(500).json({
    message: res.__('badId.copies'),
    obj: ex
  })}
  );
}

function create(req, res, next){
  const {
    format,
    movie,
    number,
    status
  } = req.body;

  let copy = new Copy({
    format: format,
    movie: movie,
    number: number,
    status: status
  });

  copy.save().then(
    obj => {
      logger.level = "info";
      logger.info(res.__('create.copies'));
      res.status(200).json({ 
      message: res.__('create.copies'),
      obj: obj
    })})
    .catch(
      ex => {
      logger.level="error";
      logger.error(res.__('badCreate.copies'));
      res.status(500).json({
      message: res.__('badCreate.copies'),
      obj: ex
    })}
    );
  }
  

function replace(req, res, next) {
  const id = req.params.id;
  const format = req.body.format ? req.body.format : "";
  const movie = req.body.movie ? req.body.movie : object.movie;
  const number = req.body.number ? req.body.number : 0;
  const status = req.body.status ? req.body.status : object.status;

  let copy = new Object({
    _format: format,
    _movie: movie,
    _number: number,
    _status: status
  });

  Copy.findOneAndUpdate({"_id":id}, copy).then(obj => 
    {
      logger.level = "info";
      logger.info(res.__('another.copies'));
      res.status(200).json({
      message: res.__('another.copies') ,
      obj: obj
    })})
    .catch(ex => {
      logger.level = "error";
      logger.info(res.__('badAnother.copies'));
      res.status(500).json({
      message: res.__('badAnother.copies'),
      obj: ex
    })}
    );
  }
  

async function edit(req, res, next) {
  const id = req.params.id;
  const {
    format,
    movie,
    number,
    status
  } = req.body;

  const copy = await Copy.findOne({"_id":id});

  if(format) {
    copy._format = format;
  }
  if(movie) {
    copy._movie = movie;
  }
  if(number) {
    copy._number = number;
  }
  if(status) {
    copy._status = status;
  }

  copy.save().then(
    obj =>{
      logger.level = "info";
      logger.info(res.__('edit.copies'));
      res.status(200).json({
      message: res.__('edit.copies'),
      obj: obj
    })})
    .catch(
      ex => {
      logger.level="error";
      logger.error(res.__('badEdit.copies'));
      res.status(500).json({
      message: res.__ ('badEdit.copies'),
      obj: ex
    })}
    );
  }

function destroy(req, res, next) {
  const id = req.params.id;
  Copy.remove({"_id":id}).then(
    obj =>{
      logger.level = "info";
      logger.info(res.__('delete.copies'));
      res.status(200).json({
      message: res.__('delete.copies'),
      obj: obj
    })})
    .catch(
      ex => {
      logger.level="error";
      logger.error(res.__('badDelete.copies'));
      res.status(500).json({
      message: res.__ ('badDelete.copies'),
      obj: ex
    })}
    );
  }

module.exports = {
  list, index, create, replace, edit, destroy
}
