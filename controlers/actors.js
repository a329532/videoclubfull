const express = require('express');
const Actor = require('../models/actor');
const config = require('config');
const user = require('../models/user');
var log4js = require("log4js");
var logger = log4js.getLogger();

// RESTFULL => GET, POST, PUT, PATCH, DELETE = Modelo
// representacion de una estructura de datos
function list(req, res, next) {
  Actor.find().then(
    objs =>{
    logger.level = "info";
    logger.info(res.__('list.actors'));
    res.status(200).json({
    message: res.__('list.actors'),
    obj: objs
  })})
  .catch(
    ex => {
    logger.level="error";
    logger.error(res.__('badList.actors'));
    res.status(500).json({
    message: res.__ ('badList.actors'),
    obj: ex
  })}
  );
}

function index(req, res, next) {
  const id = req.params.id;
  Actor.findOne({"_id":id}).then(obj => {
    logger.level = "info";
    logger.info(res.__('id.actors'));
    res.status(200).json({
    message: res.__('id.actors'),
    obj: obj
  })})
  .catch(
    ex =>{
    logger.level="error";
    logger.error(res.__('badId.actors'));
    res.status(500).json({
    message: res.__('badId.actors'),
    obj: ex
  })}
  );
}

function create(req, res, next){
  const name = req.body.name;
  const lastName = req.body.lastName;

  let actor = new Actor({
    name: name,
    lastName: lastName
  });

  actor.save().then(
    obj => {
    logger.level = "info";
    logger.info(res.__('create.actors'));
    res.status(200).json({ 
    message: res.__('create.actors'),
    obj: obj
  })})
  .catch(
    ex => {
    logger.level="error";
    logger.error(res.__('badCreate.actors'));
    res.status(500).json({
    message: res.__('badCreate.actors'),
    obj: ex
  })}
  );
}

function replace(req, res, next) {
  const id = req.params.id;
  const name = req.body.name ? req.body.name : "";
  const lastName = req.body.lastName ? req.body.lastName : "";

  let actor = new Object({
    _name:name,
    _lastName: lastName
  });

  Actor.findOneAndUpdate({"_id":id}, actor).then(obj =>
    {
    logger.level = "info";
    logger.info(res.__('another.actors'));
    res.status(200).json({
    message: res.__('another.actors') ,
    obj: obj
  })})
  .catch(ex => {
    logger.level = "error";
    logger.info(res.__('badAnother.actors'));
    res.status(500).json({
    message: res.__('badAnother.actors'),
    obj: ex
  })}
  );
}

function edit(req, res, next) {
  const id = req.params.id;
  const name = req.body.name;
  const lastName = req.body.lastName;

  let actor = new Object();

  if(name) {
    actor._name = name;
  }
  if(lastName) {
    actor._lastName = lastName;
  }
  Actor.findOneAndUpdate({"_id":id}, actor).then(
    obj =>{
    logger.level = "info";
    logger.info(res.__('edit.actors'));
    res.status(200).json({
    message: res.__('edit.actors'),
    obj: obj
  })})
  .catch(
    ex => {
    logger.level="error";
    logger.error(res.__('badEdit.actors'));
    res.status(500).json({
    message: res.__ ('badEdit.actors'),
    obj: ex
  })}
  );
}

function destroy(req, res, next) {
  const id = req.params.id;
  Actor.remove({"_id":id}).then(
    obj =>{
      logger.level = "info";
      logger.info(res.__('delete.actors'));
      res.status(200).json({
      message: res.__('delete.actors'),
      obj: obj
    })})
    .catch(
      ex => {
      logger.level="error";
      logger.error(res.__('badDelete.actors'));
      res.status(500).json({
      message: res.__ ('badDelete.actors'),
      obj: ex
    })}
    );
  }
module.exports = {
  list, index, create, replace, edit, destroy
}
