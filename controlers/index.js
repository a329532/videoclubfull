const express = require('express');
const bcrypt = require('bcrypt');
const async = require('async');
const jwt = require('jsonwebtoken');
const config = require('config');
var log4js = require("log4js");
var logger = log4js.getLogger();

const User = require('../models/user');

const jwtKey = config.get("secret.key");

function home(req, res, next) {
  res.render('index', { title: 'Express' });
}

function login(req, res, next) {
  let email = req.body.email;
  let password = req.body.password;

  async.parallel({
    user: callback => User.findOne({_email:email})
    .select('_password _salt')
    .exec(callback)
  }, (err, result) => {
    if(result.user) {
      bcrypt.hash(password, result.user.salt, (err, hash) => {
      logger.level = "info";
      logger.info(res.__('ok.login'));
        if(hash === result.user.password) {
          res.status(200).json({
            message: res.__('ok.login'),
            "obj": jwt.sign(result.user.id, jwtKey)
          });
        } else {
          logger.level="error";
           logger.error(res.__('bad.login'));
          res.status(403).json({
            message:res.__('bad.login'),
             "obj":null
          });
        }
      });
    } else {
      res.status(403).json({
        message:res.__('bad.login'),
         "obj":null
      });
    }
  });
}

module.exports = {
  home, login
};
