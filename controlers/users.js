const express = require('express');
const bcrypt = require('bcrypt');
const async = require('async');
var log4js = require("log4js");
var logger = log4js.getLogger();

const User = require('../models/user');

var hashed;

// RESTFULL => GET, POST, PUT, PATCH, DELETE = Modelo
// representacion de una estructura de datos
function list(req, res, next) {
  let page = req.params.page ? req.params.page : 1;
  User.paginate({}, {page:page, limit:config.paginate.size}).then(objs =>
    {
      logger.level = "info";
      logger.info(res.__('list.users'));
      res.status(200).json({
      message: res.__('list.users'),
      obj: objs
    })})
    .catch(
      ex => {
      logger.level="error";
      logger.error(res.__('list.users'));
      res.status(500).json({
      message: res.__ ('badList.users'),
      obj: ex
    })}
    );
  }
  

function index(req, res, next) {
  const id = req.params.id;
  User.findOne({"_id":id}).then(obj => 
    {
      logger.level = "info";
      logger.info(res.__('id.actors'));
      res.status(200).json({
      message: res.__('id.actors'),
      obj: objs
    })})
    .catch(
      ex => {
      logger.level="error";
      logger.error(res.__('badId.actors'));
      res.status(500).json({
      message: res.__ ('badId.actors'),
      obj: ex
    })}
    );
  }

async function create(req, res, next){
  const name = req.body.name;
  const lastName = req.body.lastName;
  const email = req.body.email;
  const password = req.body.password;

  async.parallel({
    salt:(callback) => {
      bcrypt.genSalt(10, callback);
    }
  }, (err, result) => {
    bcrypt.hash(password, result.salt, async (err, hash) =>{
      hashed = hash;

      let user = new User({
        name: name,
        lastName: lastName,
        email: email,
        password: hash,
        salt: result.salt,
      });

      await user.save().then(obj => {
        logger.level = "info";
        logger.info(res.__('create.actors'));
        res.status(200).json({
          message: res.__('create.users'),
          obj: obj
        });
      }).catch(ex =>
        {
        logger.level = "error";
        logger.info(res.__('badCreate.actors'));
          res.status(500).json({
         
        message: "res__('badCreate.users')",
        obj: ex
      })});

    });

  });
}

function replace(req, res, next) {
  const id = req.params.id;
  const name = req.body.name ? req.body.name : "";
  const lastName = req.body.lastName ? req.body.lastName : "";
  const email = req.body.email ? req.body.email : "";
  const password = req.body.password ? req.body.password : "";
  const permissions = req.body.permissions ? req.body.permissions : object.permissions;

  let user = new Object({
    _name: name,
    _lastName: lastName,
    _email: email,
    _password: password,
    _permissions: permissions
  });

  User.findOneAndUpdate({"_id":id}, user).then(obj => {
    logger.level = "info";
    logger.info(res.__('another.user'));
    res.status(200).json({
    message: res.__('anothe.user'),
    obj: obj
  })})
  .catch(
    ex =>{
    logger.level="error";
    logger.error(res.__('badAnother.user'));
    res.status(500).json({
    message: res.__('badAnother.user'),
    obj: ex
  })}
  );
}

function edit(req, res, next) {
  const id = req.params.id;
  const name = req.body.name;
  const lastName = req.body.lastName;
  const email = req.body.email;
  const password = req.body.password;
  const permissions = req.body.permissions;

  let user = new Object();

  if(name) {
    user._name = name;
  }
  if(lastName) {
    user._lastName = lastName;
  }
  if(email) {
    user._email = email;
  }
  if(password) {
    user._password = password;
  }
  if(permissions) {
    user._permissions = permissions;
  }
  User.findOneAndUpdate({"_id":id}, user).then(obj =>{
    logger.level = "info";
    logger.info(res.__('edit.users'));
    res.status(200).json({
    message: res.__('edit.users'),
    obj: obj
  })}).catch(
    ex =>{
    logger.level="error";
    logger.error(res.__('badEdit.users'));
    res.status(500).json({
    message: res.__('badEdit.users'),
    obj: ex
  })}
  );
}

function destroy(req, res, next) {
  const id = req.params.id;
  User.remove({"_id":id}).then(obj => {
    logger.level = "info";
    logger.info(res.__('delete.users'));
    res.status(200).json({
    message: res.__('delete.users'),
    obj: obj
  })})
  .catch(
    ex =>{
    logger.level="error";
    logger.error(res.__('badDelete.users'));
    res.status(500).json({
    message: res.__('badDelete.users'),
    obj: ex
  })}
  );
}

module.exports = {
  list, index, create, replace, edit, destroy
}
