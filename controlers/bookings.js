const express = require('express');
const Booking = require('../models/booking');
var log4js = require("log4js");
var logger = log4js.getLogger();

// RESTFULL => GET, POST, PUT, PATCH, DELETE = Modelo
// representacion de una estructura de datos
function list(req, res, next) {
  Booking.find().populate("_copy").populate("_member").then(objs => {
    logger.level = "info";
    logger.info(res.__('list.bookings'));
    res.status(200).json({
    message: res.__('list.bookings'),
    obj: objs
  })})
  .catch(
    ex =>{
    logger.level="error";
    logger.error(res.__('badList.bookings'));
    res.status(500).json({
    message: res.__('badList.bookings'),
    obj: ex
  })}
  );
}
function index(req, res, next) {
  const id = req.params.id;
  Booking.findOne({"_id":id}).populate("_copy").populate("_member").then(obj => {
    logger.level = "info";
    logger.info(res.__('id.bookings'));
    res.status(200).json({
    message: res.__('id.bookings'),
    obj: obj
  })})
  .catch(
    ex =>{
    logger.level="error";
    logger.error(res.__('badId.bookings'));
    res.status(500).json({
    message: res.__('badId.bookings'),
    obj: ex
  })}
  );
}

function create(req, res, next){
  const {
    copy,
    member,
    date
  } = req.body;

  let booking = new Booking({
    copy: copy,
    member: member,
    date: date
  });

  booking.save().then(obj => {
    logger.level = "info";
    logger.info(res.__('create.bookings'));
    res.status(200).json({
    message: res.__('create.bookings'),
    obj: obj
  })})
  .catch(
    ex =>{
    logger.level="error";
    logger.error(res.__('badCreate.bookings'));
    res.status(500).json({
    message: res.__('badCreate.bookings'),
    obj: ex
  })}
  );
}

function replace(req, res, next) {
  const id = req.params.id;
  const copy = req.body.copy ? req.body.copy : object.copy;
  const member = req.body.member ? req.body.member : object.member;
  const date = req.body.date ? req.body.date : object.date;

  let booking = new Object({
    _copy: copy,
    _member: member,
    _date: date
  });

  Booking.findOneAndUpdate({"_id":id}, booking).then(obj => {
    logger.level = "info";
    logger.info(res.__('another.bookings'));
    res.status(200).json({
    message: res.__('another.bookings'),
    obj: obj
  })})
  .catch(
    ex =>{
    logger.level="error";
    logger.error(res.__('badAnother.bookings'));
    res.status(500).json({
    message: res.__('badAnother.bookings'),
    obj: ex
  })}
  );
}

async function edit(req, res, next) {
  const id = req.params.id;
  const {
    copy,
    member,
    date
  } = req.body;

  const booking = await Booking.findOne({"_id":id});

  if(copy) {
    booking._copy = copy;
  }
  if(member) {
    booking._member = member;
  }
  if(date) {
    booking._date = date;
  }

  booking.save().then(
    obj =>{
      logger.level = "info";
      logger.info(res.__('edit.users'));
      res.status(200).json({
      message: res.__('edit.users'),
      obj: obj
    })})
    .catch(
      ex => {
      logger.level="error";
      logger.error(res.__('badEdit.users'));
      res.status(500).json({
      message: res.__ ('badEdit.users'),
      obj: ex
    })}
    );
}  

function destroy(req, res, next) {
  const id = req.params.id;
  Booking.remove({"_id":id}).then(
    obj =>{
      logger.level = "info";
      logger.info(res.__('delete.bookings'));
      res.status(200).json({
      message: res.__('delete.bookings'),
      obj: obj
    })})
    .catch(
      ex => {
      logger.level="error";
      logger.error(res.__('badDelete.bookings'));
      res.status(500).json({
      message: res.__ ('badDelete.bookings'),
      obj: ex
    })}
    );
  }

module.exports = {
  list, index, create, replace, edit, destroy
}
