const supertest = require('supertest');

const app = require('../app');

var key = "";
var id = "";

// sentencia
describe('Probar el sistema de autenticacion', ()=>{
  // casos de prueba => 50%
    it('Deberia de obtener un login con usuario y contrasena correcto', (done)=>{
        supertest(app).post('/login')
        .send({
            "email" : "donmeko@ugo.com",
            "password" : "orawe"
        })
        .expect(200) // Status HTTP
        .end(function(err, res){
            key = res.body.obj;
            done();
        });
    });
});
describe('Probar las rutas de los members', ()=>{
    it('Deberia de crear un member', (done)=>{
        supertest(app).post('/members/')
        .set('Authorization', `Bearer ${key}`)
        .send({
            name: "el ete",
            lastName: "el ete",
            phone: "el ete",
            status: true,
            city: "el ete",
            country: "el ete",
            number: "el ete",
            state: "el ete",
            street: "el ete"
        })
        .end(function(err, res){
        if(err){
            done(err);
        } else {
            id = res.body.obj._id;
            expect(res.statusCode).toEqual(200);
            done();
        }
        })
    });
    it('Deberia de enlistar members', (done)=>{
        supertest(app).get(`/members/`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            } else {
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    });
    it('Deberia de encontrar un member', (done)=>{
        supertest(app).get(`/members/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            } else {
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    });
    it('Deberia de editar member', (done)=>{
        supertest(app).patch(`/members/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .send({
            name: "el ete",
            lastName: "el ete",
            phone: "el ete",
            status: true,
            city: "el ete",
            country: "el ete",
            number: "el ete",
            state: "el ete",
            street: "el ete"
        })
        .end(function(err, res){
        if(err){
            done(err);
        } else {
            expect(res.statusCode).toEqual(200);
            done();
        }
        })
    });
    it('Deberia de reemplazar un member', (done)=>{
        supertest(app).put(`/members/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .send({
            name: "el ete",
            lastName: "el ete",
            phone: "el ete",
            status: true,
            city: "el ete",
            country: "el ete",
            number: "el ete",
            state: "el ete",
            street: "el ete"
        })
        .end(function(err, res){
        if(err){
            done(err);
        } else {
            expect(res.statusCode).toEqual(200);
            done();
        }
        })
    });
    it('Deberia de borrar un member', (done)=>{
        supertest(app).delete(`/bookings/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
        if(err){
            done(err);
        } else {
            expect(res.statusCode).toEqual(200);
            done();
        }
        })
    });
});
