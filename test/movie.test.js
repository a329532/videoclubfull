const supertest = require('supertest');

const app = require('../app');

var key = "";
var id = "";

// sentencia
describe('Probar el sistema de autenticacion', ()=>{
  // casos de prueba => 50%
    it('Deberia de obtener un login con usuario y contrasena correcto', (done)=>{
        supertest(app).post('/login')
        .send({
            "email" : "miau@miau.com",
            "password" : "yes"
        })
        .expect(200) // Status HTTP
        .end(function(err, res){
            key = res.body.obj;
            done();
        });
    });
});
describe('Probar las rutas de los movies', ()=>{
    it('Deberia de crear un movie', (done)=>{
        supertest(app).post('/movies/')
        .set('Authorization', `Bearer ${key}`)
        .send({
            director: "6177659e1577c92d6e71529d",
            actors: ["6173b5f72054e9ef906e5605"],
            genre: "Horror",
            title: "Scream"
        })
        .end(function(err, res){
        if(err){
            done(err);
        } else {
            id = res.body.obj._id;
            expect(res.statusCode).toEqual(200);
            done();
        }
        })
    });
    it('Deberia de enlistar movies', (done)=>{
        supertest(app).get(`/movies/`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            } else {
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    });
    it('Deberia de encontrar un movie', (done)=>{
        supertest(app).get(`/movies/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            } else {
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    });
    it('Deberia de editar movie', (done)=>{
        supertest(app).patch(`/movies/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .send({
            director: "6177659e1577c92d6e71529d",
            actors: ["6173b5f72054e9ef906e5605"],
            genre: "Comedy",
            title: "Scream"
        })
        .end(function(err, res){
        if(err){
            done(err);
        } else {
            expect(res.statusCode).toEqual(200);
            done();
        }
        })
    });
    it('Deberia de reemplazar un movie', (done)=>{
        supertest(app).put(`/movies/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .send({
            director: "6177659e1577c92d6e71529d",
            actors: ["6173b5f72054e9ef906e5605"],
            genre: "Yes",
            title: "Yes"
        })
        .end(function(err, res){
        if(err){
            done(err);
        } else {
            expect(res.statusCode).toEqual(200);
            done();
        }
        })
    });
    it('Deberia de borrar un movie', (done)=>{
        supertest(app).delete(`/movies/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
        if(err){
            done(err);
        } else {
            expect(res.statusCode).toEqual(200);
            done();
        }
        })
    });
});

