const supertest = require('supertest');

const app = require('../app');

var key = "";
var id = "";

// sentencia
describe('Probar el sistema de autenticacion', ()=>{
  // casos de prueba => 50%
    it('Deberia de obtener un login con usuario y contrasena correcto', (done)=>{
        supertest(app).post('/login')
        .send({
            "email" : "miau@miau.com",
            "password" : "yes"
        })
        .expect(200) // Status HTTP
        .end(function(err, res){
            key = res.body.obj;
            done();
        });
    });
});
describe('Probar las rutas de los copies', ()=>{
    it('Deberia de crear un copy', (done)=>{
        supertest(app).post('/copies/')
        .set('Authorization', `Bearer ${key}`)
        .send({
            format: "BlueRay",
            movie: "6177659e1577c92d6e71529d",
            number: 3,
            status: true
        })
        .end(function(err, res){
        if(err){
            done(err);
        } else {
            id = res.body.obj._id;
            expect(res.statusCode).toEqual(200);
            done();
        }
        })
    });
    it('Deberia de enlistar copies', (done)=>{
        supertest(app).get(`/copies/`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            } else {
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    });
    it('Deberia de encontrar un copy', (done)=>{
        supertest(app).get(`/copies/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            } else {
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    });
    it('Deberia de editar copy', (done)=>{
        supertest(app).patch(`/copies/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .send({
            format: "BlueRay",
            movie: "6177659e1577c92d6e71529d",
            number: 3,
            status: true
        })
        .end(function(err, res){
        if(err){
            done(err);
        } else {
            expect(res.statusCode).toEqual(200);
            done();
        }
        })
    });
    it('Deberia de reemplazar un copy', (done)=>{
        supertest(app).put(`/copies/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .send({
            format: "BlueRay",
            movie: "6177659e1577c92d6e71529d",
            number: 3,
            status: true
        })
        .end(function(err, res){
        if(err){
            done(err);
        } else {
            expect(res.statusCode).toEqual(200);
            done();
        }
        })
    });
    it('Deberia de borrar un copy', (done)=>{
        supertest(app).delete(`/copies/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
        if(err){
            done(err);
        } else {
            expect(res.statusCode).toEqual(200);
            done();
        }
        })
    });
});
