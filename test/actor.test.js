const supertest = require('supertest');

const app = require('../app');

var key = "";
var id = "";

// sentencia
describe('Probar el sistema de autenticacion', ()=>{
  // casos de prueba => 50%
    it('Deberia de obtener un login con usuario y contrasena correcto', (done)=>{
        supertest(app).post('/login')
        .send({
            "email" : "miau@miau.com",
            "password" : "yes"
        })
        .expect(200) // Status HTTP
        .end(function(err, res){
            key = res.body.obj;
            done();
        });
    });
});
describe('Probar las rutas de los actores', ()=>{
    it('Deberia de crear actor', (done)=>{
        supertest(app).post('/actors/')
        .set('Authorization', `Bearer ${key}`)
        .send({
            name: "yo1",
            lastname: "mero1"
        })
        .end(function(err, res){
        if(err){
            done(err);
        } else {
            id = res.body.obj._id;
            expect(res.statusCode).toEqual(200);
            done();
        }
        })
    });
    it('Deberia de enlistar actores', (done)=>{
        supertest(app).get(`/actors/`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            } else {
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    });
    it('Deberia de encontrar un actor', (done)=>{
        supertest(app).get(`/actors/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            } else {
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    });
    it('Deberia de editar actor', (done)=>{
        supertest(app).patch(`/actors/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .send({
            name: "yo2",
            lastname: "mero2"
        })
        .end(function(err, res){
        if(err){
            done(err);
        } else {
            expect(res.statusCode).toEqual(200);
            done();
        }
        })
    });
    it('Deberia de reemplazar actor', (done)=>{
        supertest(app).put(`/actors/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .send({
            name: "yo3",
            lastname: "mero3"
        })
        .end(function(err, res){
        if(err){
            done(err);
        } else {
            expect(res.statusCode).toEqual(200);
            done();
        }
        })
    });
    it('Deberia de borrar actor', (done)=>{
        supertest(app).delete(`/actors/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
        if(err){
            done(err);
        } else {
            expect(res.statusCode).toEqual(200);
            done();
        }
        })
    });
});
