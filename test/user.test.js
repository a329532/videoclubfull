const supertest = require('supertest');

const app = require('../app');

var key = "";

// sentencia
describe('Probar el sistema de autenticacion', ()=>{
  // casos de prueba => 50%
  it('Deberia de obtener un login con usuario y contrasena correcto', (done)=>{
    supertest(app).post('/login')
    .send({
      "email" : "miau@miau.com",
      "password" : "yes"
    })
    .expect(200) // Status HTTP
    .end(function(err, res){
      key = res.body.obj;
      done();
    });
  });
});

describe('Probar las rutas de los usuarios', ()=>{
  it('Deberia de obtener la lista de usuarios', (done)=>{
    supertest(app).get('/users/')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      } else {
        expect(res.statusCode).toEqual(200);
        done();
      }
    })
  });
});
