const supertest = require('supertest');

const app = require('../app');

var key = "";
var id = "";

// sentencia
describe('Probar el sistema de autenticacion', ()=>{
  // casos de prueba => 50%
    it('Deberia de obtener un login con usuario y contrasena correcto', (done)=>{
        supertest(app).post('/login')
        .send({
            "email" : "miau@miau.com",
            "password" : "yes"
        })
        .expect(200) // Status HTTP
        .end(function(err, res){
            key = res.body.obj;
            done();
        });
    });
});
describe('Probar las rutas de los bookings', ()=>{
    it('Deberia de crear bookings', (done)=>{
        supertest(app).post('/bookings/')
        .set('Authorization', `Bearer ${key}`)
        .send({
            copy: "61789ffee8de2804cc111cdc",
            member: "61775a4c8bc79a8a45a1141b"
        })
        .end(function(err, res){
        if(err){
            done(err);
        } else {
            id = res.body.obj._id;
            expect(res.statusCode).toEqual(200);
            done();
        }
        })
    });
    it('Deberia de enlistar bookings', (done)=>{
        supertest(app).get(`/bookings/`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            } else {
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    });
    it('Deberia de encontrar un bookings', (done)=>{
        supertest(app).get(`/bookings/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            } else {
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    });
    it('Deberia de editar bookings', (done)=>{
        supertest(app).patch(`/bookings/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .send({
            copy: "61789ffee8de2804cc111cdc",
            member: "61775a4c8bc79a8a45a1141b"
        })
        .end(function(err, res){
        if(err){
            done(err);
        } else {
            expect(res.statusCode).toEqual(200);
            done();
        }
        })
    });
    it('Deberia de reemplazar bookings', (done)=>{
        supertest(app).put(`/bookings/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .send({
            copy: "61789ffee8de2804cc111cdc",
            member: "61775a4c8bc79a8a45a1141b"
        })
        .end(function(err, res){
        if(err){
            done(err);
        } else {
            expect(res.statusCode).toEqual(200);
            done();
        }
        })
    });
    it('Deberia de borrar bookings', (done)=>{
        supertest(app).delete(`/bookings/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
        if(err){
            done(err);
        } else {
            expect(res.statusCode).toEqual(200);
            done();
        }
        })
    });
});
